import random

def read_entries(filename):
    with open(filename, "r") as f:
        return [int(line.rstrip()) for line in f]

def part1_bruteforce(A, n, sum):
    # Consider all possible pairs
    # and check their sums
    for i in range(0, n):
        for j in range(i + 1, n):
            if A[i] + A[j] == sum:
                print('Part 1 Bruteforce Pairs: {} {}'.format(A[i], A[j]))
                return

def part2_bruteforce(A, n, sum):
    # Consider all possible pairs
    # and check their sums
    for i in range(0, n):
        for j in range(i + 1, n):
            for k in range(j + 1, n):
                if A[i] + A[j] + A[k] == sum:
                    print('Part 2 Bruteforce Triplets: {} {} {}'.format(A[i], A[j], A[k]))
                    return

def part1_binarysearch(A, sum):
    # Sort the list in ascending order
    A.sort()

    # Maintain two indices pointing to end-points of the list
    (low, high) = (0, len(A) - 1)

    # Reduce search space A[low...high] at each iteration of the loop

    # Loop till low is less than high
    while low < high:
        if A[low] + A[high] == sum: # Sum found
                print('Part 1 Binary Search Pairs: {} {}'.format(A[low], A[high]))
                return

        # Increment low index if total is less than the desired sum
        # Decrement high index is total is more than the sum
        if A[low] + A[high] < sum:
            low = low + 1
        else:
            high = high - 1
    
    # No pair found
    print("Pair not found")

entries = read_entries("beeg.txt")
part1_bruteforce(entries, len(entries), 2020)
part1_binarysearch(entries, 2020)
part2_bruteforce(entries, len(entries), 2020)