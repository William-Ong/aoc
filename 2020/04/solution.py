import re

def passport_validation(k_v):
    # Split k_v pair into two individual variable
    key, value = k_v
    if key == 'byr':
        return 1920 <= int(value) <= 2002
    elif key == 'iyr':
        return 2010 <= int(value) <= 2020
    elif key == 'eyr':
        return 2020 <= int(value) <= 2030
    elif key == 'hgt':
        # Get the height by removing the unit from the end
        # Get the unit type by referring to the last two index
        try:
            height, unit = int(value[:-2]), value[-2:]
        except ValueError:
            return False
        # Validate height using ternary operator
        return 150 <= height <= 193 if unit == 'cm' else 59 <= height <= 76
    elif key == 'hcl':
        # Check hair colour via regex
        # Pattern: Starts with '#' with a set of characters that is alphanumeric at
        #          exactly 6 occurences
        # Source: https://www.w3schools.com/python/python_regex.asp
        return bool(re.fullmatch(r'#[0-9a-f]{6}', value))
    elif key == 'ecl':
        return value in 'amb blu brn gry grn hzl oth'.split()
    elif key == 'pid':
        return len(value) == 9 and value.isdigit()
    return True

part_1 = part_2 = 0
passport = dict()
attr = {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}

# Read the input file and splits on the newline character, adding a
# blank line at the end of the file to consider the last passport
# as well
# Source: https://www.webucator.com/how-to/how-read-file-with-python.cfm
for line in open("input.txt").read().splitlines() + ['']:
    # Go through each attribute of the passport until it hits a blank line,
    # signalling that the passport itself is being processed
    if line:
        # Update the empty passport dictionary with the input contents
        # using list comprehension
        passport.update(dict(k_v.split(':') for k_v in line.split()))
    # Blank line hit! Validate passport...
    else:
        # Check if the passport has valid attributes
        if attr.issubset(passport.keys()):
            part_1 += 1
            # all(): Return True if bool(x) is True for all values x in the iterable. 
            #        If the iterable is empty, return True.
            # map(): map(func, *iterables) --> map object
            #        Make an iterator that computes the function using arguments from 
            #        each of the iterables. Stops when the shortest iterable is exhausted.
            if all(map(passport_validation, passport.items())):
                part_2 += 1
        # Reset dictionary
        passport = dict()

print(part_1)
print(part_2)