from functools import reduce

def read_entries(filename):
    with open(filename, "r") as f:
        return [line.rstrip() for line in f]

def find_tree(entries, right, down):
    tree_encountered = 0
    # Get the height and width of the entire map
    height, width = len(entries), len(entries[0])
    
    # Add a counter for each row. Note: i == row
    for i, row in enumerate(range(0, height, down)):
        # Finds the specific column in which the row left off with respect
        # to the width size and the number of steps required.
        # Instruction: From the current index, go right 3 steps and down 1 step.
        col = (right*i % width)

        # If tree is found in that specific spot, increment counter
        if entries[row][col] == '#':
            tree_encountered += 1

    return tree_encountered

# Get data
entries = read_entries("input.txt")

# Part 1
print(find_tree(entries, 3, 1))

# Part 2
slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
product = reduce(lambda x, y: x * y, [find_tree(entries, *tup) for tup in slopes])
print(product)