import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Solution
{
    public static void main(String [] args)
    {
        String filename = "input.txt";
        try
        {
            List<String> list = read(filename);
            System.out.println(findTree(list));
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public static List<String> read(String filename) throws IOException
    {
        List<String> result;
        try (Stream<String> lines = Files.lines(Paths.get(filename)))
        {
            result = lines.collect(Collectors.toList());
        }
        return result;
    }

    public static int findTree(List<String> map)
    {
        int tree_counter = 0;
        int width = map.get(0).length();
        int row = 0;
        for(String mapRow : map)
        {
            int col = 3 * row % width;
            if(mapRow.charAt(col) == '#')
            {
                tree_counter++;
            }
            row++;
        }
        return tree_counter;
    }
}