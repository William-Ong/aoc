def read_entries(filename):
    with open(filename, "r") as f:
        return [line.rstrip() for line in f]

def detect(entries):
    part1_count = part2_count = 0
    for entry in entries:
        # Split off entry string into different sections
        num, char, password = entry.split()

        # Map the low and high character counts by splitting it when met with '-' character
        low, high = map(int, num.split('-'))

        # Slice off the ':' from the matching character
        char = char[:-1]

        # If the character matched is in range
        if low <= password.count(char) <= high:
            part1_count = part1_count + 1
        
        # Using XOR ensure that only one condition determines the other
        if (password[low - 1] == char) ^ (password[high - 1] == char):
            part2_count = part2_count + 1

    print('Part 1 matches: {}'.format(part1_count))
    print('Part 2 matches: {}'.format(part2_count))

entries = read_entries("input.txt")
detect(entries)